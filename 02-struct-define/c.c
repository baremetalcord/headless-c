//cc -lm c.c rect.c
#include <stdio.h>

typedef struct rect rect;

extern rect* draw_rectangle(float x, float y, float height, float width);
extern void log_params_rectangle(const rect * const rectangle);
extern void destroy_rectangle(rect* rectangle);

int main(int argc, char** argv){
   rect* rectangle = draw_rectangle(1.0f, 1.0f, 1.0f, 1.0f);
   log_params_rectangle(rectangle);
   destroy_rectangle(rectangle);
}
