#include <math.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct{
   float x; 
   float y;
   float height;
   float width;
} rect;

float diameter_rectangle(const rect* const rectangle){
  return sqrtf(((rectangle->height*rectangle->height)+(rectangle->width*rectangle->width)));
}

rect* draw_rectangle(float x, float y, float height, float width){
   rect* rectangle = malloc(sizeof(rect));
      rectangle->x = x;
      rectangle->y = y;
      rectangle->height = height;
      rectangle->width = width;
   return rectangle;
}

void log_params_rectangle(const rect * const rectangle){
   printf("Rect(%f,%f): Height=%f Width=%f\n",rectangle->x, rectangle->y, rectangle->height, rectangle->width);
   printf("Rect(%f,%f): Diameter=%f\n", rectangle->x, rectangle->y, diameter_rectangle(rectangle));
}

void destroy_rectangle(rect* rectangle){
   free(rectangle);
}
