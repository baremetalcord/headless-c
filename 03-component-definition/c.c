//cc -lm c.c canvas.c canvas/rect.c canvas/circle.c
#include <stdio.h>
typedef struct object object;

extern object* draw_rect(float x, float y, float h, float w);
extern object* draw_circle(float x, float y, float r);
extern void log_object(object* o);
extern void remove_everything(void);

int main(int argc, char** argv){
   object* rect = draw_rect(1.0f, 1.0f, 1.0f, 1.0f);
   object* circle = draw_circle(0.0f, 0.0f, 1.0f);

   log_object(rect);
   log_object(circle);

   remove_everything();
}
