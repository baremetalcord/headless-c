#include <stddef.h>
#define CANVAS_SIZE 10

typedef struct {
   float x; 
   float y;
   float h;
   float w;
} rect;
typedef struct circle circle;

extern rect* rect_new(float x, float y, float h, float w);
extern void rect_log(rect* r);
extern void rect_drop(rect* r);
extern circle* circle_new(float x, float y, float r);
extern void circle_log(circle* c);
extern void circle_drop(circle* c);

typedef enum{
   NO_TYPE,
   RECT,
   CIRCLE,
} object_type;

typedef struct{
   object_type type;
   union{
      rect* r;
      circle* c;
   };
} object;

static object canvas[CANVAS_SIZE] = {0};
static size_t cursor = 0;

object* draw_rect(float x, float y, float h, float w){
   object* current_object = &canvas[cursor++];
   current_object->type = RECT;
   current_object->r = rect_new(x,y,h,w);
   return current_object;
}

object* draw_circle(float x, float y, float r){
   object* current_object = &canvas[cursor++];
   current_object->type = CIRCLE;
   current_object->c = circle_new(x,y,r);
   return current_object;
}

void log_object(object* o){
   switch(o->type){
      case RECT: rect_log(o->r); break;
      case CIRCLE: circle_log(o->c); break;
      default:break;
   }
}

void remove_everything(void){
   for(size_t i = 0; i < CANVAS_SIZE; i++){
      switch(canvas[i].type){
         case RECT: rect_drop(canvas[i].r); break;
         case CIRCLE: circle_drop(canvas[i].c); break;
         default:break;
      }
   }
}
