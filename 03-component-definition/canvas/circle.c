#include <math.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct{
   float x; 
   float y;
   float r;
} circle;

circle* circle_new(float x, float y, float r){
   circle* c = malloc(sizeof(circle));
      c->x = x;
      c->y = y;
      c->r = r;
   return c;
}

void circle_log(const circle * const c){
   printf("Circle(%f,%f): Radius=%f\n",c->x, c->y, c->r);
}

void circle_drop(circle* c){
   free(c);
}
