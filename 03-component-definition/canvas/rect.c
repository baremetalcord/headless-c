#include <math.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct{
   float x; 
   float y;
   float h;
   float w;
} rect;

//float diameter_rectangle(const rect* const rectangle){
//  return sqrtf(((rectangle->h*rectangle->h)+(rectangle->w*rectangle->w)));
//}

rect* rect_new(float x, float y, float h, float w){
   rect* rectangle = malloc(sizeof(rect));
      rectangle->x = x;
      rectangle->y = y;
      rectangle->h = h;
      rectangle->w = w;
   return rectangle;
}

void rect_log(const rect * const rectangle){
   printf("Rect(%f,%f): Height=%f Width=%f\n",rectangle->x, rectangle->y, rectangle->h, rectangle->w);
}

void rect_drop(rect* rectangle){
   free(rectangle);
}
