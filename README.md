# Examples of constructing c code without usage of header files

... unless it limits to the inner code. This means that all dependencies are included normally.

# But it's ok, right?

Libraries are typically meant to used anyware in the code, so using them with headerfiles is not violating the method. In fact, you can construct a library that internally does not use headers to pass around it's public functions, yet create header for API.

This approach is not meant to go extreme (since it is still possible to define needs as extern functions to, for example, libc).

# Ok but how can you code like this?

In repo structure there are examples to check. Each of them will contain compilation command to check. Be my guest to make a quest and explore this topic further.
